package com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.ui.wallet;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.MainActivity;
import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.R;
import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.data.AppExecutors;
import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.data.CryptoRoomDatabase;
import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.model.Crypto;

import java.util.List;

/**
 * Class representing the Wallet Fragment
 */
public class WalletFragment extends Fragment {

    WalletAdapter walletAdapter;
    private List<Crypto> cryptoList;
    private CryptoRoomDatabase mDb;
    TextView tvTotalWalletPrice;

    /**
     * Create and returns the view hierarchy associated with the Wallet Fragment
     *
     * @param inflater           LayoutInflater: The LayoutInflater object that can be used to inflate any views in the fragment,
     * @param container          ViewGroup
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        WalletViewModel walletViewModel = new ViewModelProvider(requireActivity()).get(WalletViewModel.class);
        View root = inflater.inflate(R.layout.fragment_wallet, container, false);
        walletViewModel.getCryptoList().observe(getViewLifecycleOwner(), new Observer<List<Crypto>>() {
            @Override
            public void onChanged(List<Crypto> cryptos) {
                cryptoList = cryptos;
                walletAdapter.setWalletList(cryptos);
                walletAdapter.notifyDataSetChanged();
                updateTotalWallet();
            }
        });
        return root;
    }


    /**
     * @param view               The View returned by onCreateView
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous saved state as given here. This value may be null.
     **/
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDb = CryptoRoomDatabase.getDatabase(getActivity());
        RecyclerView rvWallet = view.findViewById(R.id.rv_wallet);
        rvWallet.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvWallet.setHasFixedSize(true);
        walletAdapter = new WalletAdapter();
        rvWallet.setAdapter(walletAdapter);

        // écouteur pour le click sur une ligne
        // provient de l'interface CurrenciesAdapter.onItemClickListener
        walletAdapter.setOnClickListener(new WalletAdapter.onItemClickListener() {
            /**
             * Remove the Crypto object at the given position, out of the Wallet Adapter by setting
             * isInWallet to false when the delete option is clicked from the contextual menu
             * @param position The position of the Crypto object in the Adapter List
             */
            @Override
            public void onClickDelete(int position) {
                Crypto currentCrypto = cryptoList.get(position);
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mDb.cryptoDao().putCryptoInWallet(false, currentCrypto.getId());
                    }
                });
            }

            /**
             * Edit the Crypto object from the adapter and the database at the given position when the edit option is clicked
             * @param position The position of the Crypto object in the Adapter List
             */
            @Override
            public void onClickEdit(int position) {
                Crypto currentCrypto = cryptoList.get(position);
                ((MainActivity) getActivity()).editCrypto(currentCrypto, position);
            }
        });
    }

    /**
     * Update a Crypto object following an add or edit option
     *
     * @param crypto   Crypto object to edit or add
     * @param position The position of the Crypto object in the adapter list
     */
    public void updateCrypto(Crypto crypto, int position) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                if (position > -1) {
                    mDb.cryptoDao().update(crypto);
                    cryptoList.remove(position);
                } else {
                    mDb.cryptoDao().insert(crypto);
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        cryptoList.add(crypto);
                        walletAdapter.notifyDataSetChanged();
                    }
                });
            }
        });
    }

    /**
     * Update the UI TextView tvTotalWalletPrice representing the total value of the wallet.
     */
    private void updateTotalWallet() {
        Float totalWallet = 0f;
        tvTotalWalletPrice = getActivity().findViewById(R.id.tv_totalWallet);
        for (Crypto crypto : cryptoList
        ) {
            totalWallet += crypto.getTotalPrice();
        }
        tvTotalWalletPrice.setText(getActivity().getResources().getString(R.string.wallet_Total, totalWallet));
    }
}