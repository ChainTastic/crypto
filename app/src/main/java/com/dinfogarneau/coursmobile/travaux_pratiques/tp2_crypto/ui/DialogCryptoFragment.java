package com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.R;
import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.model.Crypto;
import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.ui.currencies.CurrenciesFragment;
import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.ui.wallet.WalletAdapter;
import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.ui.wallet.WalletFragment;


public class DialogCryptoFragment extends DialogFragment {
    private Crypto crypto;
    private int position = -1;

    public DialogCryptoFragment() {
    }

    public DialogCryptoFragment(Crypto crypto, int position) {
        this.crypto = crypto;
        this.position = position;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        String textButtonAdd = getString(R.string.textButtonAddCrypto);
        if (position > -1) {
            textButtonAdd = getString(R.string.textButtonEditCrypto);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        final View v = inflater.inflate(R.layout.fragment_add_crypto_form, null);
        final Context context = getContext();

        if (crypto != null) {
            EditText etName = v.findViewById(R.id.et_name);
            EditText etSymbole = v.findViewById(R.id.et_symbole);
            EditText etPrice = v.findViewById(R.id.et_price);
            EditText etAmount = v.findViewById(R.id.et_amount);
            etName.setText(crypto.getName());
            etSymbole.setText(crypto.getSymbol());
            etPrice.setText(Integer.toString(crypto.getPrice() * 100));
            etAmount.setText(Float.toString(crypto.getAmount()));
        }

        builder.setView(v)
                .setPositiveButton(textButtonAdd, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        EditText etName = v.findViewById(R.id.et_name);
                        EditText etSymbol = v.findViewById(R.id.et_symbole);
                        EditText etPrice = v.findViewById(R.id.et_price);
                        EditText etAmount = v.findViewById(R.id.et_amount);

                        String name = etName.getText().toString().trim();
                        String symbol = etSymbol.getText().toString().trim();
                        String price = etPrice.getText().toString().trim();
                        String amount = etAmount.getText().toString().trim();

                        if (name.isEmpty() || symbol.isEmpty() || price.isEmpty() || amount.isEmpty()) {
                            Toast.makeText(context, getResources().getString(R.string.error_crypto_dialog), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (crypto != null) {
                            crypto.setName(name);
                            crypto.setSymbol(symbol);
                            crypto.setPrice(Integer.parseInt(price) / 100);
                            crypto.setAmount(Float.parseFloat(amount));
                        } else {
                            crypto = new Crypto(name, symbol, Integer.parseInt(price) / 100, Float.parseFloat(amount));
                        }

                        Fragment currentFragment = (Fragment) requireActivity().getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment).getChildFragmentManager().getFragments().get(0);
                        if (currentFragment instanceof CurrenciesFragment) {
                            CurrenciesFragment currenciesFragment = (CurrenciesFragment) currentFragment;
                            currenciesFragment.updateCrypto(crypto, position);
                        }
                        else if (currentFragment instanceof WalletFragment) {
                            WalletFragment walletFragment = (WalletFragment) currentFragment;
                            walletFragment.updateCrypto(crypto, position);
                        }

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        if (position > -1) {
            builder.setTitle(R.string.modify_crypto_dialog_title);
        } else {
            builder.setTitle(R.string.add_crypto_dialog_title);
        }
        return builder.create();
    }

}
