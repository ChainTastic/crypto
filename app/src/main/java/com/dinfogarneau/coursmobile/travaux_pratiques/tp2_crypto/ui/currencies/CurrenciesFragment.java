package com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.ui.currencies;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.MainActivity;
import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.R;
import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.data.AppExecutors;
import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.data.CryptoRoomDatabase;
import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.model.Crypto;
import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.ui.DialogCryptoFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

/**
 * Class representing the Currencies Fragment
 */
public class CurrenciesFragment extends Fragment {

    private boolean adminMode = false;
    private FloatingActionButton fabAdd;
    private CryptoRoomDatabase mDb;
    public CurrenciesAdapter currenciesAdapter;
    public List<Crypto> cryptoList;

    /**
     * Create and returns the view hierarchy associated with the Currencies Fragment
     *
     * @param inflater           LayoutInflater: The LayoutInflater object that can be used to inflate any views in the fragment,
     * @param container          ViewGroup
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        CurrenciesViewModel currenciesViewModel = new ViewModelProvider(requireActivity()).get(CurrenciesViewModel.class);
        View root = inflater.inflate(R.layout.fragment_currencies, container, false);

        currenciesViewModel.getCryptoList().observe(getViewLifecycleOwner(), new Observer<List<Crypto>>() {
            @Override
            public void onChanged(List<Crypto> cryptos) {
                cryptoList = cryptos;
                currenciesAdapter.setCurrenciesList(cryptos);
                currenciesAdapter.notifyDataSetChanged();
            }
        });

        return root;
    }

    /**
     * @param view               The View returned by onCreateView
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous saved state as given here. This value may be null.
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDb = CryptoRoomDatabase.getDatabase(getActivity());

        fabAdd = view.findViewById(R.id.fab_addCurrencies);
        RecyclerView rvCurrency = view.findViewById(R.id.rv_currencies);
        rvCurrency.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvCurrency.setHasFixedSize(true);
        currenciesAdapter = new CurrenciesAdapter();
        rvCurrency.setAdapter(currenciesAdapter);

        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogCryptoFragment newCryptoFragment = new DialogCryptoFragment();
                newCryptoFragment.show(getParentFragmentManager(), "AddCrypto");
            }
        });


        // écouteur pour le click sur une ligne
        // provient de l'interface CurrenciesAdapter.onItemClickListener
        currenciesAdapter.setOnClickListener(new CurrenciesAdapter.onItemClickListener() {
            /**
             * Send the Crypto item in the wallet
             * @param position The position of the Crypto object in the adapter
             */
            @Override
            public void onItemClick(int position) {
                Crypto crypto = cryptoList.get(position);
                int cryptoID = crypto.getId();
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mDb.cryptoDao().putCryptoInWallet(true, cryptoID);
                    }
                });
            }

            /**
             * Delete the Crypto from the Adapter and the database at given position when the delete
             * option is clicked from the contextual menu
             * @param position The position of the Crypto object in the adapter list
             */
            @Override
            public void onClickDelete(int position) {
                Crypto crypto = cryptoList.get(position);
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mDb.cryptoDao().deleteACrypto(crypto.getId());
                        cryptoList.remove(position);
                        currenciesAdapter.notifyItemRemoved(position);
                    }
                });
            }

            /**
             * Edit the Crypto from the Adapter and the database at given position when the edit
             * option is clicked from the contextual menu
             * @param position The position of the Crypto object in the adapter list
             */
            @Override
            public void onClickEdit(int position) {
                Crypto currentCrypto = cryptoList.get(position);
                ((MainActivity) getActivity()).editCrypto(currentCrypto, position);
            }
        });
    }

    /**
     * Trigger if the Admin mode is activated or not
     */
    public void triggerAdminMode() {
        Log.d("TAG", "triggerAdminMode: ");
        if (!adminMode) {
            fabAdd.setVisibility(View.VISIBLE);
            adminMode = true;
            Toast.makeText(getActivity(), getResources().getString(R.string.adminMode_activated_message), Toast.LENGTH_SHORT).show();
        } else {
            fabAdd.setVisibility(View.INVISIBLE);
            adminMode = false;
            Toast.makeText(getActivity(), getResources().getString(R.string.adminMode_desactivated_message), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Update a Crypto object following an add or edit option
     *
     * @param crypto   Crypto object to edit or add
     * @param position The position of the Crypto object in the adapter list
     */
    public void updateCrypto(Crypto crypto, int position) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                if (position > -1) {
                    mDb.cryptoDao().update(crypto);
                    cryptoList.remove(position);
                } else {
                    mDb.cryptoDao().insert(crypto);
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        cryptoList.add(crypto);
                        currenciesAdapter.notifyDataSetChanged();
                    }
                });
            }
        });
    }

    /**
     * Delete all the Crypto object from the adapter and the database
     */
    public void deleteAll() {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mDb.cryptoDao().deleteAll();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        currenciesAdapter.notifyItemRangeRemoved(0, cryptoList.size());
                    }
                });
            }
        });

    }

}