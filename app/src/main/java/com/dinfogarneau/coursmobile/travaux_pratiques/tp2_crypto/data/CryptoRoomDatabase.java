package com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.data;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.model.Crypto;

/**
 * Class that represent the RoomDatabase for the Crypto model
 */
@Database(entities = {Crypto.class}, version = 1)
public abstract class CryptoRoomDatabase extends RoomDatabase {

    // Singleton
    public static volatile CryptoRoomDatabase INSTANCE;
    // DAO
    public abstract CryptoDao cryptoDao();

    /**
     * Create the room database
     * @param context
     * @return return a CryptoRoomDatabase
     */
    public static CryptoRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            // Crée la BDD
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    CryptoRoomDatabase.class, "crypto_database")
                    .build();
        }
        return INSTANCE;
    }


}
