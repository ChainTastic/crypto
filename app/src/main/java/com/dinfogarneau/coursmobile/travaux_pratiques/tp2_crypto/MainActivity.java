package com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.data.AppExecutors;
import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.data.CryptoRoomDatabase;
import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.model.Crypto;
import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.ui.DialogCryptoFragment;
import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.ui.currencies.CurrenciesFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    private static final int CAMERA_PERMISSION_CODE = 123;
    private CryptoRoomDatabase mDb;

    /**
     * Called when the activity is starting.
     *
     * @param savedInstanceState If the activity is being re-initialized after previously being
     *                           shut down then this Bundle contains the data it most recently
     *                           supplied in onSaveInstanceState(Bundle). Note: Otherwise it is null.
     *                           This value may be null.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_wallet, R.id.navigation_currencies)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);


        mDb = CryptoRoomDatabase.getDatabase(getApplicationContext());

        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                if (mDb.cryptoDao().getSize() <= 0) {
                    Crypto ethereum = new Crypto("Ethereum", "ETH", 15000, 5f);
                    Crypto bitcoin = new Crypto("Bitcoin", "BTC", 50000, 2.5f);
                    mDb.cryptoDao().insert(ethereum);
                    mDb.cryptoDao().insert(bitcoin);
                }
            }
        });

    }

    /**
     * Initialize the contents of the Activity's standard options menu.
     *
     * @param menu The options menu in which you place your items.
     * @return You must return true for the menu to be displayed; if you return false it will not be shown.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     *
     * @param item The menu item that was selected. This value cannot be null.
     * @return Return false to allow normal menu processing to proceed, true to consume it here.
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.mn_admin:
                if (ContextCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{
                            Manifest.permission.CAMERA}, CAMERA_PERMISSION_CODE);
                } else {
                    Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment).getChildFragmentManager().getFragments().get(0);
                    if (currentFragment instanceof CurrenciesFragment) {
                        CurrenciesFragment currenciesFragment = (CurrenciesFragment) currentFragment;
                        currenciesFragment.triggerAdminMode();
                    }
                }
                return true;
            case R.id.mn_deleteAll:
                Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment).getChildFragmentManager().getFragments().get(0);
                if (currentFragment instanceof CurrenciesFragment) {
                    CurrenciesFragment currenciesFragment = (CurrenciesFragment) currentFragment;
                    currenciesFragment.deleteAll();
                    Toast.makeText(this, MainActivity.this.getString(R.string.deleted_all_message), Toast.LENGTH_SHORT).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Callback for the result from requesting permissions.
     *
     * @param requestCode  The code of the requested permission.
     * @param permissions  The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_PERMISSION_CODE) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted. Continue the action or workflow in your app
                CurrenciesFragment currentFragment = (CurrenciesFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment).getChildFragmentManager().getFragments().get(0);
                if (currentFragment instanceof CurrenciesFragment) {
                    currentFragment.triggerAdminMode();
                }
            } else if (grantResults.length > 0 &&
                    grantResults[0] == PackageManager.PERMISSION_DENIED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                        Manifest.permission.CAMERA)) {

                    AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                    setupPermissionAlert(dialog);
                    dialog.show();
                }
            }
        }
    }

    /**
     * Setup the alerts Dialog for the given permission
     *
     * @param dialog AlertDialog
     */
    private void setupPermissionAlert(AlertDialog.Builder dialog) {
        dialog.setTitle(R.string.permission_alert_title);
        dialog.setMessage(R.string.permission_alert_message);
        dialog.setPositiveButton(R.string.alert_btn_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                requestPermissions(new String[]{
                        Manifest.permission.CAMERA}, CAMERA_PERMISSION_CODE);
            }
        });
        dialog.setNegativeButton(R.string.alert_btn_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(
                        MainActivity.this,
                        MainActivity.this.getString(R.string.alert_denied_access),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Open up the DialogCryptoFragment to create or edit a Crypto object
     *
     * @param crypto   Crypto object
     * @param position position of the object in the list of the adapter
     */
    public void editCrypto(Crypto crypto, int position) {
        DialogCryptoFragment dialogCryptoFragment = new DialogCryptoFragment(crypto, position);
        dialogCryptoFragment.show(getSupportFragmentManager(), "AddCrypto");
    }


}