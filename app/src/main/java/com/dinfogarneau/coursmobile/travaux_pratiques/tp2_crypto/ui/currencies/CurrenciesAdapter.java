package com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.ui.currencies;

import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.R;
import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.model.Crypto;

import java.util.List;

public class CurrenciesAdapter extends RecyclerView.Adapter<CurrenciesAdapter.CurrenciesViewHolder> {

    private List<Crypto> currenciesList;
    private onItemClickListener mListener;

    // Interface pour gestion du click sur une ligne
    public interface onItemClickListener {
        void onItemClick(int position);

        void onClickDelete(int position);

        void onClickEdit(int position);
    }

    /**
     * Set the OnItemClickListener interface
     *
     * @param listener OnItemClickListener interface
     */
    public void setOnClickListener(onItemClickListener listener) {
        this.mListener = listener;
    }

    /**
     * Set the list of the Crypto object of the currencies adapter
     *
     * @param currenciesList List of Crypto Object
     */
    public void setCurrenciesList(List<Crypto> currenciesList) {
        this.currenciesList = currenciesList;
    }


    /**
     * Class representing the Currencies RecyclerView Fragment ViewHolder
     */
    public static class CurrenciesViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public TextView tvSymbole;
        public TextView tvPrice;
        public ImageView ivLogo;

        public CurrenciesViewHolder(@NonNull View itemView, final onItemClickListener listener) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_currencyName);
            tvSymbole = itemView.findViewById(R.id.tv_currencySymbol);
            tvPrice = itemView.findViewById(R.id.tv_currencyPrice);
            ivLogo = itemView.findViewById(R.id.img_currencyLogo);

            // écouteur sur le clic sur un ligne
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        // Bonne pratique pour s'assurer que l'élément clické est toujours présent
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });

            // écouteur pour clic long sur une ligne pour menu contextuel
            itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                @Override
                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                    MenuItem edit = menu.add(0, v.getId(), 0, R.string.edit);
                    MenuItem delete = menu.add(0, v.getId(), 1, R.string.delete);

                    delete.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            int position = getAdapterPosition();
                            listener.onClickDelete(position);
                            return false;
                        }
                    });

                    edit.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            int position = getAdapterPosition();
                            listener.onClickEdit(position);
                            return false;
                        }
                    });
                }
            });
        }
    }

    /**
     * Initialize the Currencies ViewHolder
     *
     * @param parent   parent ViewGroup of the ViewHolder
     * @param viewType The viewType of the ViewHolder
     * @return CurrenciesViewHolder object
     */
    @NonNull
    @Override
    public CurrenciesAdapter.CurrenciesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.currency_one_line, parent, false);
        return new CurrenciesViewHolder(view, mListener);
    }

    /**
     * Bind the ViewHolder to the Currencies Adapter
     *
     * @param holder   ViewHolder
     * @param position Position of the viewHolder in the list
     */
    @Override
    public void onBindViewHolder(@NonNull CurrenciesAdapter.CurrenciesViewHolder holder, int position) {

        if (currenciesList != null) {
            Crypto current = currenciesList.get(position);
            holder.tvName.setText(current.getName());
            holder.tvSymbole.setText(current.getSymbol().toUpperCase());
            Float floatPrice = (float) current.getPrice();
            holder.tvPrice.setText(holder.itemView.getContext().
                    getString(R.string.txt_CryptoPrice, floatPrice));

            int resourceId = holder.itemView.getResources().
                    getIdentifier(
                            current.getSymbol().toLowerCase(),
                            "drawable",
                            "com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto"
                    );
            holder.ivLogo.setImageResource(resourceId);
        }
    }


    /**
     * Returns the size of the collection that contains the items we want to display
     *
     * @return Integer representing size of the collection that contains the items we want to display
     */
    @Override
    public int getItemCount() {
        if (currenciesList != null) {
            return currenciesList.size();
        } else return 0;
    }
}
