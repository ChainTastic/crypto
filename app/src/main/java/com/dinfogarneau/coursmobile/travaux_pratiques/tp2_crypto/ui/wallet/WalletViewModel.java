package com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.ui.wallet;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.data.CryptoRoomDatabase;
import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.model.Crypto;

import java.util.List;

public class WalletViewModel extends AndroidViewModel {

    private LiveData<List<Crypto>> cryptoList;
    private CryptoRoomDatabase mDb;

    public WalletViewModel(Application application) {
        super(application);
        mDb = CryptoRoomDatabase.getDatabase(application);
        cryptoList = mDb.cryptoDao().getAllWalletCryptos();
    }

    /**
     * Method that return a LiveData List of Crypto object in the wallet
     *
     * @return LiveData List of Crypto object
     */
    public LiveData<List<Crypto>> getCryptoList() {
        if (cryptoList == null) {
            cryptoList = new MutableLiveData<>();

            loadWallet(getApplication());
        }
        return cryptoList;
    }

    /**
     * Method that load the CryptoList that are in the wallet from the database.
     *
     * @param application
     */
    private void loadWallet(Application application) {
        mDb = CryptoRoomDatabase.getDatabase(application);
        cryptoList = mDb.cryptoDao().getAllWalletCryptos();
    }
}