package com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.model.Crypto;

import java.util.List;

@Dao
public interface CryptoDao {
    /**
     * Insert a crypto to the database
     * @param crypto a crypto Object
     */
    @Insert
    void insert(Crypto crypto);

    /**
     * Delete all the crypto from the database
     */
    @Query("DELETE FROM crypto_table")
    void deleteAll();

    /**
     * Delete a specific crypto with the specified ID
     * @param id id of the object Crypto
     */
    @Query("DELETE FROM crypto_table WHERE id = :id")
    void deleteACrypto(int id);

    /**
     * Update the crypto query in the database according to the crypto object received in parameters
     * @param crypto crypto object
     */
    @Update
    void update(Crypto crypto);

    /**
     * return all the crypto inside the database sorted by name in descending order
     * @return LiveData List of all Crypto object inside the database
     */
    @Query("SELECT * FROM crypto_table ORDER BY name DESC")
    LiveData<List<Crypto>> getAllCryptos();

    /**
     * Return the amount of crypto object in the database
     * @return
     */
    @Query("SELECT COUNT(*) FROM crypto_table")
    Integer getSize();

    /**
     * Return all the crypto inside the database that are present in the wallet
     * @return LiveData List of all Crypto object with the isInWallet attribute to true.
     */
    @Query("SELECT * FROM crypto_table WHERE isInWallet = 1")
    LiveData<List<Crypto>> getAllWalletCryptos();

    /**
     * Update a crypto's isInWallet attribute either by true or false
     * @param isInWallet Is the crypto is in the wallet (true) or not (false)
     * @param id The id of the crypto object
     */
    @Query("UPDATE crypto_table SET isInWallet = :isInWallet WHERE id = :id")
    void putCryptoInWallet(boolean isInWallet, int id);
}
