package com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Class that represent a Crypto object
 */
@Entity(tableName = "crypto_table")
public class Crypto {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    @ColumnInfo(name = "name")
    private String name;

    @NonNull
    @ColumnInfo(name = "symbol")
    private String symbol;

    @NonNull
    @ColumnInfo(name = "price")
    private Integer price;

    @NonNull
    @ColumnInfo(name = "amount")
    private Float amount;

    @NonNull
    @ColumnInfo(name = "isInWallet")
    private Boolean isInWallet;

    public Crypto(String name, String symbol, Integer price, Float amount) {
        this.name = name;
        this.symbol = symbol;
        this.price = price;
        this.amount = amount;
        this.isInWallet = false;
    }

    /**
     * return the id of the Crypto object
     *
     * @return The id of the crypto object
     */
    public int getId() {
        return id;
    }

    /**
     * Set the id of the Crypto object
     *
     * @param id int representing the desired id for the Crypto object
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Return the name of the Crypto object
     *
     * @return the name of the Crypto object
     */
    @NonNull
    public String getName() {
        return name;
    }

    /**
     * Set the name of the Crypto object
     *
     * @param name String representing the desired name for the Crypto object
     */
    public void setName(@NonNull String name) {
        this.name = name;
    }

    /**
     * Return the symbol of the Crypto object
     *
     * @return String representing the symbol of the Crypto object
     */
    @NonNull
    public String getSymbol() {
        return symbol;
    }

    /**
     * Set the symbol of the Crypto object
     *
     * @param symbol String representing the symbol of the Crypto object
     */
    public void setSymbol(@NonNull String symbol) {
        this.symbol = symbol;
    }

    /**
     * Return the price of the Crypto object
     *
     * @return Integer representing the price of the Crypto object
     */
    @NonNull
    public Integer getPrice() {
        return price;
    }

    /**
     * Set the price of the Crypto object
     *
     * @param price Integer representing the price of the Crypto object
     */
    public void setPrice(@NonNull Integer price) {
        this.price = price;
    }

    /**
     * Return the amount of the Crypto object
     *
     * @return Float representing the amount of the Crypto object
     */
    @NonNull
    public Float getAmount() {
        return amount;
    }

    /**
     * Set the amount of the Crypto object
     *
     * @param amount Float representing the amount of the Crypto object
     */
    public void setAmount(@NonNull Float amount) {
        this.amount = amount;
    }

    /**
     * Return if the Crypto object is in the wallet (true) or not (false)
     *
     * @return Boolean representing if the Crypto is in the wallet (true) or not (false)
     */
    @NonNull
    public Boolean getInWallet() {
        return isInWallet;
    }

    /**
     * Set if the Crypto object is in the wallet (true) or not (false)
     *
     * @param inWallet Boolean representing if the Crypto is in the wallet (true) or not (false)
     */
    public void setInWallet(@NonNull Boolean inWallet) {
        isInWallet = inWallet;
    }

    /**
     * Return the total price of a crypto using it's amount and price
     *
     * @return Float of the multiplication of the amount and price of the Crypto object
     */
    public Float getTotalPrice() {
        return this.getAmount() * this.getPrice();
    }
}
