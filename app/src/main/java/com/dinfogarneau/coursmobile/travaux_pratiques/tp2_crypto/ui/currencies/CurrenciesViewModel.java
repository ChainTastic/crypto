package com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.ui.currencies;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.data.CryptoRoomDatabase;
import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.model.Crypto;

import java.util.List;

public class CurrenciesViewModel extends AndroidViewModel {

    private LiveData<List<Crypto>> cryptoList;
    private CryptoRoomDatabase mDb;

    public CurrenciesViewModel(Application application) {
        super(application);
        mDb = CryptoRoomDatabase.getDatabase(application);
        cryptoList = mDb.cryptoDao().getAllCryptos();
    }

    /**
     * Method that return a LiveData List of Crypto object
     *
     * @return LiveData List of Crypto object
     */
    public LiveData<List<Crypto>> getCryptoList() {
        if (cryptoList == null) {
            cryptoList = new MutableLiveData<>();

            loadCryptos(getApplication());
        }
        return cryptoList;
    }

    /**
     * Method that load every Crypto object from the database
     *
     * @param application
     */
    private void loadCryptos(Application application) {
        mDb = CryptoRoomDatabase.getDatabase(application);
        cryptoList = mDb.cryptoDao().getAllCryptos();
    }

}