package com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.ui.wallet;

import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.R;
import com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto.model.Crypto;

import java.util.List;


public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.WalletViewHolder> {

    private List<Crypto> walletList;
    private onItemClickListener mListener;

    // Interface pour gestion du click sur une ligne
    public interface onItemClickListener {
        void onClickDelete(int position);

        void onClickEdit(int position);
    }

    /**
     * Set the OnItemClickListener interface
     *
     * @param listener OnItemClickListener interface
     */
    public void setOnClickListener(onItemClickListener listener) {
        this.mListener = listener;
    }

    /**
     * Set the list of the Crypto object of the wallet adapter
     *
     * @param walletList List of Crypto Object
     */
    public void setWalletList(List<Crypto> walletList) {
        this.walletList = walletList;
    }

    /**
     * Class representing the Wallet RecyclerView Fragment ViewHolder
     */
    public static class WalletViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public TextView tvSymbol;
        public TextView tvPrice;
        public TextView tvTotalPrice;
        public TextView tvAmount;
        public ImageView ivLogo;

        public WalletViewHolder(@NonNull View itemView, final onItemClickListener listener) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_currencyName);
            tvSymbol = itemView.findViewById(R.id.tv_currencySymbol);
            tvPrice = itemView.findViewById(R.id.tv_currencyPrice);
            tvTotalPrice = itemView.findViewById(R.id.tv_totalPrice);
            tvAmount = itemView.findViewById(R.id.tv_qtty);
            ivLogo = itemView.findViewById(R.id.img_currencyLogo);

            // écouteur pour clic long sur une ligne pour menu contextuel
            itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                @Override
                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                    MenuItem edit = menu.add(0, v.getId(), 0, R.string.edit);
                    MenuItem delete = menu.add(0, v.getId(), 1, R.string.delete);

                    delete.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            int position = getAdapterPosition();
                            listener.onClickDelete(position);
                            return false;
                        }
                    });

                    edit.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            int position = getAdapterPosition();
                            listener.onClickEdit(position);
                            return false;
                        }
                    });
                }
            });
        }
    }

    /**
     * Initialize the Wallet ViewHolder
     *
     * @param parent   parent ViewGroup of the ViewHolder
     * @param viewType The viewType of the ViewHolder
     * @return WalletViewHolder object
     */
    @NonNull
    @Override
    public WalletAdapter.WalletViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.wallet_one_line, parent, false);
        return new WalletViewHolder(view, mListener);
    }

    /**
     * Bind the ViewHolder to the Wallet Adapter
     *
     * @param holder   ViewHolder
     * @param position Position of the viewHolder in the list
     */
    @Override
    public void onBindViewHolder(@NonNull WalletAdapter.WalletViewHolder holder, int position) {
        if (walletList != null) {
            Crypto current = walletList.get(position);
            holder.tvName.setText(current.getName());
            holder.tvSymbol.setText(current.getSymbol());
            Float floatPrice = Float.valueOf(current.getPrice());
            holder.tvPrice.setText(holder.itemView.getContext()
                    .getString(R.string.txt_CryptoPrice, floatPrice));
            holder.tvAmount.setText(String.valueOf(current.getAmount()));
            Float totalPrice = current.getTotalPrice();
            holder.tvTotalPrice.setText(holder.itemView.getContext().getString(R.string.txt_CryptoTotalPrice, totalPrice));

            int resourceId = holder.itemView.getResources().
                    getIdentifier(
                            current.getSymbol().toLowerCase(),
                            "drawable",
                            "com.dinfogarneau.coursmobile.travaux_pratiques.tp2_crypto"
                    );
            holder.ivLogo.setImageResource(resourceId);
        }
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        if (walletList != null) {
            return walletList.size();
        } else return 0;
    }
}
